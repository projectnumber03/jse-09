package ru.shilov.tm.entity;

import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractEntity {

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    private LocalDate start;

    @Nullable
    private LocalDate finish;

    @Nullable
    private String userId;

    @NotNull
    private Status status = Status.PLANNED;

    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Проект: ").append(this.name).append("\n");
        sb.append("Описание: ").append(this.description).append("\n");
        sb.append("Дата начала: ").append(start != null ? DateTimeFormatter.ofPattern("dd.MM.yyyy").format(this.start) : "n/a").append("\n");
        sb.append("Дата окончания: ").append(finish != null ? DateTimeFormatter.ofPattern("dd.MM.yyyy").format(this.finish) : "n/a").append("\n");
        sb.append("Статус: ").append(status.description);
        return sb.toString();
    }

    @Getter
    @AllArgsConstructor
    public enum Status {

        PLANNED("Запланировано"),
        IN_PROCESS("В процессе"),
        DONE("Готово");

        private final String description;

    }

}
