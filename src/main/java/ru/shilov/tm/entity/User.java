package ru.shilov.tm.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class User extends AbstractEntity {

    @Nullable
    private String login;

    @Nullable
    private String password;

    @Nullable
    private Role role;

    @NotNull
    @Override
    public String getUserId() {
        return super.id;
    }

    @NotNull
    @Override
    public String getName() {
        return login;
    }

    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Пользователь: ").append(login).append("\n");
        sb.append("Роль: ").append(role != null && role.description != null ? role.description : "n/a");
        return sb.toString();
    }

    @Getter
    @AllArgsConstructor
    public enum Role {

        USER("Пользователь"),
        ADMIN("Администратор");

        private final String description;

    }

}
