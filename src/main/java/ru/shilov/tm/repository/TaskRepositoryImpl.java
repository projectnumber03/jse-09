package ru.shilov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.repository.ITaskRepository;
import ru.shilov.tm.entity.Task;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public final class TaskRepositoryImpl extends AbstractRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public List<Task> findByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entities.values().stream().filter(entity -> userId.equals(entity.getUserId()) && projectId.equals(entity.getProjectId())).collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<Task> findByNameOrDescription(@NotNull final String value, @NotNull final String userId) {
        return findByUserId(userId).stream()
                .filter(t -> Objects.requireNonNull(t.getName()).contains(value) || Objects.requireNonNull(t.getDescription()).contains(value))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public String getId(@NotNull final String value, @NotNull final String userId) {
        @NotNull
        final List<Task> entities = findByUserId(userId);
        return entities.size() >= Integer.parseInt(value) ? entities.get(Integer.parseInt(value) - 1).getId() : "";
    }

}
