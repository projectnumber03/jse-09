package ru.shilov.tm.command;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.context.ServiceLocator;
import ru.shilov.tm.entity.User;

import java.util.List;
import java.util.Objects;

public abstract class AbstractTerminalCommand {

    @Setter
    @Nullable
    private ServiceLocator serviceLocator;

    public abstract void execute() throws Exception;

    @NotNull
    public abstract List<User.Role> getRoles();

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    @NotNull
    public ServiceLocator getServiceLocator() {
        return Objects.requireNonNull(serviceLocator);
    }

}
