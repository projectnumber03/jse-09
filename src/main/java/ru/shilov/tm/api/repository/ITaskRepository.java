package ru.shilov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @NotNull
    List<Task> findByProjectId(@NotNull final String userId, @NotNull final String projectId);

    @NotNull
    List<Task> findByNameOrDescription(@NotNull final String value, @NotNull final String userId);

    @NotNull
    String getId(@NotNull final String value, @NotNull final String userId);

}
