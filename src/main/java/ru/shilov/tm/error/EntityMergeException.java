package ru.shilov.tm.error;

public final class EntityMergeException extends Exception {

    public EntityMergeException() {
        super("Ошибка обновления объекта");
    }

}
