package ru.shilov.tm;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.other.AboutCommand;
import ru.shilov.tm.command.other.ExitCommand;
import ru.shilov.tm.command.other.HelpCommand;
import ru.shilov.tm.command.project.*;
import ru.shilov.tm.command.task.*;
import ru.shilov.tm.command.user.*;
import ru.shilov.tm.context.Bootstrap;

public final class Application {

    public static void main(@NotNull final String[] args) throws Exception {
        new Bootstrap().init();
    }

}
