package ru.shilov.tm.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.ITaskRepository;
import ru.shilov.tm.api.service.ITaskService;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.error.EntityRemoveException;
import ru.shilov.tm.error.NoSuchEntityException;
import ru.shilov.tm.error.NumberToIdTransformException;

import java.util.List;

@AllArgsConstructor
public final class TaskServiceImpl extends AbstractService<Task> implements ITaskService {

    @Getter
    @NotNull
    private final ITaskRepository repository;

    @NotNull
    @Override
    public List<Task> findByUserId(@Nullable final String userId) throws NoSuchEntityException {
        if (userId == null || userId.isEmpty()) throw new NoSuchEntityException();
        return repository.findByUserId(userId);
    }

    @NotNull
    @Override
    public Boolean removeByUserId(@Nullable final String userId) throws EntityRemoveException {
        if (userId == null || userId.isEmpty()) throw new EntityRemoveException();
        return repository.removeByUserId(userId);
    }

    @NotNull
    @Override
    public List<Task> findByProjectId(@Nullable final String userId, @Nullable final String projectId) throws NoSuchEntityException {
        if (userId == null || userId.isEmpty() || projectId == null || projectId.isEmpty()) throw new NoSuchEntityException();
        return repository.findByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    public List<Task> findByNameOrDescription(@Nullable String value, @Nullable String userId) throws NoSuchEntityException {
        if (userId == null || userId.isEmpty() || value == null || value.isEmpty()) throw new NoSuchEntityException();
        return repository.findByNameOrDescription(value, userId);
    }

    @NotNull
    @Override
    public Boolean removeOneByUserId(@Nullable final String id, @Nullable final String userId) throws EntityRemoveException {
        if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) throw new EntityRemoveException();
        return repository.removeOneByUserId(id, userId);
    }

    @NotNull
    @Override
    public String getId(@Nullable final String value, @Nullable final String userId) throws NumberToIdTransformException {
        if (!isNumber(value) || userId == null || userId.isEmpty()) throw new NumberToIdTransformException(value);
        return repository.getId(value, userId);
    }

    private boolean isNumber(@Nullable final String value) {
        return value != null && !value.isEmpty() && value.matches("\\d+");
    }

}
